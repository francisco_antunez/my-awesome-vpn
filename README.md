# Setup to connect to the VPN

First make sure wireguard-tools is installed.

```
wg -v
```
```
wireguard-tools v1.0.20200827 - https://git.zx2c4.com/wireguard-tools/
```

### Config
___

As root copy template.conf file into ```/etc/wireguard/``` (rename the .conf if you want)

check permissions of ```/etc/wireguard``` 
```
drwx------  2 root root        45 Feb 21  2020 wireguard
```
Change permisions using
```
# chmod 700 /etc/wireguard
```
Fill in the missing information in _template_.conf with information given to you from the server.

**Address** - Address assigned from the server. (10.200.200._number_)

**ListenPort** - I think you can change this.

**PrivateKey** - Your private key that you don't show anyone.

**DNS** - Change this if you want to use your own DNS which will also not use the pihole.
### **[Peer]**
**PublicKey** - The servers public key

**PresharedKey** - The preshared key given to you by the server.

**AllowedIPs** - Which traffic to allow to the server. 0.0.0.0/0 is a catch-all to direct all traffic.

**Endpoint** - The IP of the server given elsewhere.
### Start WireGuard
___
Start the WireGuard service.
```
systemctl start wg-quick@template
```
Enable on startup.
```
systemctl enable wg-quick@template
```
make sure to replace _template_ with the name you gave your .conf file, but exclude the .conf.

## Test Connection
___
Check for the interface.
```
ip addr
```
you should now see the interface ```wg0```

Ping the gateway.
```
ping -c 5 10.200.200.1
```

## Stop WireGuard
___
Stop and remove wg0
```
systemctl stop wg-quick@template
```
Disable on startup
```
systemctl disable wg-quick@template
```
make sure to replace _template_ with the name you gave your .conf file, but exclude the .conf.